from django.shortcuts import render

response = {}
# Create your views here.
def index(request):
	response['author'] = "Volyando Belvadra"
	html = 'lab_8/lab_8.html'
	return render(request,html,response)