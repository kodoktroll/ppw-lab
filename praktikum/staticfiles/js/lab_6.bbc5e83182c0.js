$(document).ready(function(){
	$("#kodok").click(function(){
		$(".chat-body").toggle();
	});
	$("#asdf").keyup(function(e){
		if(e.which == 13){
			var textarea = document.getElementById('asdf');
			var message = document.getElementById('asdf').value;
			var old = $(".msg-insert").html();
			$(".msg-insert").html(old + '<p class="msg-send">' + message + '</p>');
			textarea.value = "";
			$("#asdf").attr('placeholder', "Press Enter");
		}
	});
	$('.my-select').select2();
});


var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = "";
  } else if (x === 'eval') {
  	if(print.value == ""){
  		return;
  	}
  	else{
	      print.value = Math.round(evil(print.value) * 10000) / 10000;
	      erase = true;
  	}
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}


var themes = [
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
];

var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
// END

